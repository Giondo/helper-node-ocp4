  - name: Set authorized keys taken from file
    authorized_key:
      user: centos
      state: present
      key: "{{ SSHKEY }}"
    tags:
      - configurehelper

  - name: Create a directory if it does not exist
    file:
      path: "{{ OCP4DIR }}"
      state: directory
      mode: '0755'
    tags:
      - configurehelper

  - name: Download Openshift Client
    get_url:
      url: https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-client-linux.tar.gz
      dest: "{{ OCP4DIR }}/openshift-client-linux.tar.gz"
    tags:
      - configurehelper

  - name: Download Openshift Install
    get_url:
      url: https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz
      dest: "{{ OCP4DIR }}/openshift-install-linux.tar.gz"
    tags:
      - configurehelper

  - name: Download Terraform Install
    get_url:
      url: https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_linux_amd64.zip
      dest: "{{ OCP4DIR }}/terraform_linux_amd64.zip"
    tags:
      - configurehelper

  - name: Check if OVA Image Exists
    stat:
      path: "{{ OCP4DIR }}/rhcos-4.4.3-x86_64-vmware.x86_64.ova"
    register: ovaimagedownladed
    tags:
      - createhelper

  - name: Download rhcos OVA Image
    get_url:
      url: https://mirror.openshift.com/pub/openshift-v4/dependencies/rhcos/latest/latest/rhcos-4.4.3-x86_64-vmware.x86_64.ova
      dest: "{{ OCP4DIR }}/rhcos-4.4.3-x86_64-vmware.x86_64.ova"
    register: ovaimage
    when: not ovaimagedownladed.stat.exists
    tags:
      - configurehelper

  - name: Extract Openshift Client
    unarchive:
      src: "{{ OCP4DIR }}/openshift-client-linux.tar.gz"
      dest: "{{ OCP4DIR }}"
      remote_src: yes
    tags:
      - configurehelper

  - name: Extract Openshift Install
    unarchive:
      src: "{{ OCP4DIR }}/openshift-install-linux.tar.gz"
      dest: "{{ OCP4DIR }}"
      remote_src: yes
    tags:
      - configurehelper

  - name: Extract Terraform Install
    unarchive:
      src: "{{ OCP4DIR }}/terraform_linux_amd64.zip"
      dest: "{{ OCP4DIR }}"
      remote_src: yes
    tags:
      - configurehelper

  - name: Copy Terraform to usr sbin
    copy:
      src: "{{ OCP4DIR }}/terraform"
      dest: "/usr/local/sbin/terraform"
      remote_src: yes
      mode: u+rwx,g+rx,o+rx
    tags:
      - configurehelper

  - name: Copy Openshift install to usr sbin
    copy:
      src: "{{ OCP4DIR }}/openshift-install"
      dest: "/usr/local/sbin/openshift-install"
      remote_src: yes
      mode: u+rwx,g+rx,o+rx
    tags:
      - configurehelper

  - name: Copy Openshift Client to usr sbin
    copy:
      src: "{{ OCP4DIR }}/oc"
      dest: "/usr/local/sbin/oc"
      remote_src: yes
      mode: u+rwx,g+rx,o+rx
    tags:
      - configurehelper

  - name: install the latest version of Apache, Haproxy  and other tools
    package:
      name:
        - httpd
        - haproxy
        - ntpdate
        - bind-utils
        - ansible
        - python2-pyvmomi
        - python-requests
        - NetworkManager-glib
      state: latest
    tags:
      - configurehelper

  - name: Put SELinux in permissive mode, logging actions that would be blocked.
    selinux:
      policy: targeted
      state: permissive
    tags:
      - configurehelper

  - name: Stop and Disable firewall
    systemd:
      name: firewalld
      state: stopped
      enabled: no
    tags:
      - configurehelper

  - name: Start service httpd
    systemd:
      name: httpd
      state: started
      enabled: yes
    tags:
      - configurehelper

  - name: Start service haproxy
    systemd:
      name: haproxy
      state: started
      enabled: yes
    tags:
      - configurehelper

  - name: apache2 listen on port 8081
    lineinfile: dest=/etc/httpd/conf/httpd.conf regexp="^Listen 80" line="Listen 8081" state=present
    notify:
      - restart httpd
    tags:
      - configurehelper

  - name: Writing haproxy cfg
    template:
      src: templates/haproxy.cfg.j2
      dest: /etc/haproxy/haproxy.cfg
    notify:
      - restart haproxy
    tags:
      - configurehelper

  - name: Gather info about VM
    vmware_guest_info:
      hostname: "{{ VMWARE_HOST }}"
      username: "{{ VMWARE_USER }}"
      password: "{{ VMWARE_PASSWORD }}"
      validate_certs: no
      datacenter: "{{ DATACENTER }}"
      name: "{{ TEMPLATENAME }}"
    delegate_to: localhost
    ignore_errors: True
    register: template_info
    tags:
      - createhelper

  - name: Check if OVA Image Exists
    stat:
      path: "{{ OCP4DIR }}/rhcos-4.4.3-x86_64-vmware.x86_64.ova"
    register: ovaimagedownladed
    tags:
      - createhelper

  - name: Deploy OVA
    vmware_deploy_ovf:
      hostname: '{{ VMWARE_HOST }}'
      username: '{{ VMWARE_USER }}'
      password: '{{ VMWARE_PASSWORD }}'
      datacenter: "{{ DATACENTER }}"
      datastore: "{{ DATASTORE }}"
      folder: "{{ DATACENTER }}/vm/{{ FOLDER }}"
      name: "{{ TEMPLATENAME }}"
      networks: "{u'VM Network':u'{{ NETWORK }}'}"
      validate_certs: no
      power_on: no
      wait_for_ip_address: no
      ovf: "{{ OCP4DIR }}/rhcos-4.4.3-x86_64-vmware.x86_64.ova"
    register: deploytemplate
    when: ovaimagedownladed.stat.exists and template_info is failed
    tags:
      - createhelper

  - name: Convertir OVA a Template
    vmware_guest:
      hostname: '{{ VMWARE_HOST }}'
      username: '{{ VMWARE_USER }}'
      password: '{{ VMWARE_PASSWORD }}'
      validate_certs: no  
      name: "{{ TEMPLATENAME }}"
      is_template: yes
    register: deployastemplate
    when: template_info is not failed
    tags:
      - createhelper      