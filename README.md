
# Helper Node

Installing Openshift is tedious and complicated although the official guide helps a lot there is always complications that is why I made this
The idea is to create all the needed services in this node and after installation those services can be moved to the enterprise counterpart 


Using this  helper node will help you install a Working Openshift  and then you can replace all the components  

This Ansible  will install and configure all the components needed to run Openshift in this helper node
It will:
- Deploy a Centos 7 Image (helper.tgz) configured only with some scripts to automate the networking part, sudo  and basic repos like epel
-  Install and configure a DNS server with  the dns zone and  all the records for each node
-  Install and configure haproxy to serve both API and user traffic 
-  Install and configure and apache server
-  Download all the necesary to deploy OCP 4 (openshift-install, openshift-client, terraform)
-  Deploy an OVA template on VMware to be used by terraform
-  Download and configure terraform ready to deploy the infrastructure
-  Generate all ignition files needed by Openshift to install it



# How to use it

- Clone this repo (preferable in a computer in the same LAN as Vcenter is)
- Generate vars files the folder "vars" contains some vars files and two "*sample" those two must be completed with your data
- ansible -i hosts/helper.inv main.yaml 
  - You can use tags to do some tasks and not all
  - "createhelper" tag will create the helper node on VMware
  - "configurehelper" Will install and configure the helper node with all the software needed
  - "configureocp" Will regenerate all config: dns records, terraform template vars,ignition files and copy them to the right place ready to execute
Ex:
```bash
[root@helper helper-node-ocp4]# ansible-playbook main.yaml  -i hosts/helper.inv

PLAY [Create Helper Node for Openshift 4] ****************************************************************************************************************************************************************************************************

...
```
------------

```bash
[root@helper helper-node-ocp4]# ansible-playbook main.yaml  -i hosts/helper.inv --tags configureocp

PLAY [Create Helper Node for Openshift 4] ****************************************************************************************************************************************************************************************************

...
```



## Requirements

If you pretend to run this you will need these libraries installed

- Ansible
- python2-pyvmomi
- python-requests
- robertdebock.dns
- robertdebock.dhcpd

```bash
yum install ansible python2-pyvmomi python-requests -y

ansible-galaxy install robertdebock.dns robertdebock.bootstrap robertdebock.core_dependencies robertdebock.dhcpd
```

References:

- https://docs.openshift.com/container-platform/4.4/installing/installing_vsphere/installing-vsphere.html

